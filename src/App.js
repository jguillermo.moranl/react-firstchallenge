import React, { Component } from 'react';
import logo from './logo.svg';
import InputComponent from './Components/Input/Input';
import OutputComponent from './Components/output/Output';
import './App.css';

class App extends Component {

  state = {
    gamers: [
      { nickname: 'vader', age: 21 }, { nickname: 'bonJK', age: 22 }, { nickname: 'JJ', age: 16 }
    ],
    select: 0
  }

  renderOutputs = () => this.state.gamers.map((datos, index) => <OutputComponent key={index} nickname={datos.nickname} age={datos.age} id={index} selectoutput={() => this.handleEventOutput(index)} />);

  handleEnventsInput = (event) => {
    const stateNow = this.state.gamers;
    stateNow[this.state.select].nickname = event.target.value;
    this.setState({
      gamers: stateNow
    });
  }

  handleEventOutput = (index) => {
    const stateNow = this.state.gamers;
    this.setState({
      gamers: stateNow,
      select: index
    });
  }

  render() {
    return (
      <div className="content-page">
        <img src={logo} className="App-logo logo" alt="logo" />
        <div className="content-app">
          <div>
            <InputComponent value={this.state.gamers[this.state.select].nickname} change={this.handleEnventsInput} />
          </div>
          <div className="outputs">
            {this.renderOutputs()}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
