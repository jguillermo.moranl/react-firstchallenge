import React from 'react';
import './Input.css';
const InputComponent = props => <input type="text" placeholder="Input Component" value={props.value} onChange={props.change} />

export default InputComponent;

