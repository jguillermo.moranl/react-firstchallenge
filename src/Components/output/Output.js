import React from 'react';
import './Output.css';

const OutputComponent = props => <div className="content-state-default" onClick={props.selectoutput}><p>nickname : <small>{props.nickname}</small></p><p>age:<small>{props.age}</small></p></div>;

export default OutputComponent;